#CALL reversar_stock_from_compras after update and anulado = 1 compraID
DROP PROCEDURE IF EXISTS reversar_stock_from_compras;
DELIMITER /
CREATE PROCEDURE reversar_stock_from_compras(compraID INT)
BEGIN
DECLARE varSucursal INT;
	UPDATE productosucursal,(SELECT 
	productosucursal.id,
	compradetalles.producto,
	compras.sucursal,
	compradetalles.cantidad,
	productosucursal.stock
	FROM compradetalles
	INNER JOIN compras ON compras.id = compradetalles.compra
	INNER JOIN productosucursal ON productosucursal.producto = compradetalles.producto AND productosucursal.sucursal = compras.sucursal
	WHERE compra = compraID) AS compra SET productosucursal.stock = (productosucursal.stock-compra.cantidad) WHERE productosucursal.id = compra.id;	
#END IF;
END