## PARAMS SELECT get_saldo(cliente_id)
BEGIN
declare saldo int;
SELECT SUM(creditos) INTO saldo FROM (
SELECT 
IFNULL(SUM(total_credito),0) as creditos
FROM 
creditos 
WHERE clientes_id = cliente_id AND (creditos.anulado = 0 OR creditos.anulado IS NULL)
UNION ALL
SELECT (IFNULL(SUM(total_pagado),0)*-1) as pagos 
FROM pagocliente
    INNER JOIN creditos on creditos.id = pagocliente.creditos_id
WHERE (creditos.anulado = 0 or creditos.anulado is null) and pagocliente.clientes_id = cliente_id AND (pagocliente.anulado = 0 or pagocliente.anulado is null)) AS saldo;
RETURN saldo;
END