## SELECT get_saldo_por_credito(_credito_id)
BEGIN
DECLARE _saldo INT;

SELECT IFNULL(SUM(saldo),0) into _saldo FROM (
SELECT 
SUM(monto_a_pagar) as saldo
FROM plan_credito 
INNER JOIN creditos on creditos.id = plan_credito.creditos_id
WHERE creditos_id = _credito and (creditos.anulado = 0 or creditos.anulado is null)
UNION ALL
SELECT 
SUM(total_pagado)*-1
FROM pagocliente WHERE creditos_id = _credito AND (anulado = 0 OR anulado IS NULL)) as saldo;

return _saldo;
END