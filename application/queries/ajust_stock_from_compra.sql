BEGIN
INSERT INTO productosucursal
(productosucursal.proveedor_id,
productosucursal.fechaalta,
productosucursal.producto,
productosucursal.sucursal,
productosucursal.lote,
productosucursal.vencimiento,
productosucursal.precio_venta,
productosucursal.precio_costo,
productosucursal.stock)
SELECT
compras.proveedor as proveedor_id,
compras.fecha,
compradetalles.producto as codigo_producto,
compras.sucursal,
compradetalles.lote,
compradetalles.vencimiento,
productos.precio_venta,
productos.precio_costo,
compradetalles.cantidad
FROM compradetalles 
inner join productos on productos.codigo = compradetalles.producto
inner join compras on compras.id = compradetalles.compra
inner join sucursales on sucursales.id = compras.sucursal
left join productosucursal on productosucursal.producto = compradetalles.producto and productosucursal.sucursal = compras.sucursal
WHERE productosucursal.producto is null
ORDER BY `productosucursal`.`producto`  ASC; 

UPDATE productos 
SET productos.precio_venta = newPrecio, productos.precio_costo = newPrecioCosto
WHERE productos.codigo = newProducto;

CALL set_stock(newProducto);
END