#CALL anularPagos(creditos_id)
BEGIN

DECLARE _anulado INT;

SELECT anulado INTO _anulado FROM creditos WHERE creditos.id = _credito;

IF _anulado = 1 THEN
	UPDATE pagocliente SET anulado = 1 WHERE creditos_id = _credito;
END IF;

END