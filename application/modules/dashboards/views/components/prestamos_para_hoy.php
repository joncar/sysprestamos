
<?php 
	$where = '';
	if(!empty($_GET['cliente'])){
		$where = "AND (clientes.nombres LIKE '%".$_GET['cliente']."%' OR clientes.apellidos LIKE '%".$_GET['cliente']."%' OR clientes.nro_documento LIKE '%".$_GET['cliente']."%')";
	}
	$creditos = $this->db->query("
		SELECT 
		clientes.id,
		creditos.id as credito,
		CONCAT(clientes.nombres,' ',clientes.apellidos) as cliente,
		plan_credito.nro_cuota as cuota,
		FORMAT(plan_credito.monto_a_pagar,'de_DE') as importe,
		DATE_FORMAT(plan_credito.fecha_a_pagar,'%d/%m/%Y') as fecha_a_pagar,
		FORMAT(get_saldo(clientes.id),0,'de_DE') as saldo
		FROM plan_credito
		INNER JOIN creditos ON creditos.id = plan_credito.creditos_id
		INNER JOIN clientes ON clientes.id = creditos.clientes_id
		WHERE pagado = 0 AND fecha_a_pagar <= DATE(NOW()) AND user_id = ".$this->user->id." ".$where."
		GROUP BY creditos.id
		ORDER BY clientes.nombres
	");
?>

	
<?php if(empty($_GET['creditos_id'])): ?>
<div class="clientesPrestamos">
	<div class="col-xs-12 col-md-4">
		<div class="widget-box">
			<div class="widget-header widget-header-flat widget-header-small">
				<h5 class="widget-title">
					<i class="ace-icon fa fa-search"></i>
					Buscar cliente
				</h5>
				<div class="widget-toolbar no-border">
					<div class="inline dropdown-hover">						
					</div>
				</div>
			</div>
			<div class="widget-body">
				<div class="widget-main" style="padding-bottom: 0;">					
					<form action="" onsubmit="return search(this)">
						<input type="text" class="form-control" name="cliente" placeholder="Cédula, Nombre o apellidos" value="<?= @$_GET['cliente'] ?>">
						<div style="margin: 13px 0;text-align: center;">
  							<button type="submit" class="btn btn-success">
  								<i class="fa fa-search"></i> Buscar
  							</button>
  						</div>
					</form>
				</div>
			</div><!-- /.widget-body -->
		</div>
	</div>
<?php if($creditos->num_rows()==0): ?>
	<div class="col-xs-12 col-md-4">
		<div class="widget-box">
			<div class="widget-body">
				<div class="widget-main" style="/*! padding-bottom: 0; */">					
					Sin resultados a mostrar
				</div>
			</div><!-- /.widget-body -->
		</div>
	</div>
<?php endif ?>
<?php foreach($creditos->result() as $c): ?>
	<div class="col-xs-12 col-md-2">
		<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
		    <div class="widget-header">
		        
		        <h5 class="widget-title"><?= $c->cliente ?></h5><div class="widget-toolbar">
		            <a data-action="collapse" href="#">
		                <i class="ace-icon fa fa-chevron-up"></i>
		            </a>
		        </div>
		    </div>

		    <div class="widget-body"  style="cursor:pointer;" onclick="verCliente(<?= $c->credito ?>)">
		        
		    	<div class="widget-main no-padding">
		            <div style="margin-left: 0;margin-right: 0;" class="widget-main row"> 
						
		            	<div class="itemdiv memberdiv col-xs-12">
							<div class="user">
								<img alt="Joe Doe's avatar" src="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/cropper/vacio.png">
							</div>

							<div class="body">
								<div class="name">
									<a href="#">Cuota #<?= $c->cuota ?> - <?= $c->importe ?></a>
								</div>

								<div class="time">
									<i class="ace-icon fa fa-clock-o"></i>
									<span class="green"><?= $c->fecha_a_pagar ?></span><br/>
									<i class="ace-icon fa fa-money"></i>
									<span class="green" title="Saldo actual"><?= $c->saldo ?></span>
								</div>

								<div>
									<span class="label label-warning label-sm">Pendiente</span>
								</div>
							</div>
						</div>
					</div>
		        </div>
		    </div>
		</div>
	</div>
<?php endforeach ?>



<div class="modal fade" tabindex="-1" role="dialog" id="creditoCobro">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        	<span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="clienteModal">Pedro marecos</h4>
      </div>
      <div class="modal-body">
		  
      </div>
      <div class="modal-footer">
      	<span style="float:left">Saldo actual: <span id="CreditoSaldo" style="font-weight: bold"></span></span>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	function verCliente(cliente){
		$("#creditoCobro .modal-body").html('Cargando');
		$("#creditoCobro").modal('show');
		$.post('dashboards/refresh/prestamos_para_hoy?creditos_id='+cliente,{},function(data){
            $("#creditoCobro .modal-body").html(data);
        });
		
	}

	function search(f){
		var cliente = $(".clientesPrestamos input[name='cliente']").val();
		$.post('dashboards/refresh/prestamos_para_hoy?cliente='+cliente,{},function(data){
            $(".clientesPrestamos").html(data);
        });
		return false;
	}
</script>

</div>





<!------------------------- Viene por POST ----------------------------->
<?php else: ?>
	<style>
		.tab-pane#home table tr{
			cursor: pointer;
		}
	</style>
	<?php 
		$this->db->select("plan_credito.id,nro_cuota as '#Cuota',fecha_a_pagar,DATE_FORMAT(fecha_a_pagar,'%d/%m') AS vencimiento,monto_a_pagar as Importe,pagado");
		$qr = $this->db->get_where('plan_credito',array('creditos_id'=>$_GET['creditos_id']));
		$this->db->select("creditos.*,clientes.id as cliente_id, CONCAT(clientes.nombres,' ',clientes.apellidos) as cliente, FORMAT(get_saldo(clientes.id),0,'de_DE') as saldo",FALSE);
		$this->db->join('clientes','clientes.id = creditos.clientes_id');
		$credito = $this->db->get_where('creditos',array('creditos.id'=>$_GET['creditos_id']))->row();
	?>
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Selecciona una cuota</a></li>
	    <li role="presentation" id="cargarPagoTabBtn" style="display: none;"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Cargar pago</a></li>
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="home" style="max-height: 66vh;overflow-y: auto;">
	    	<div style="text-align: left">
	    		Cuota: 
				<span class="label label-success">Pagada</span>
				<span class="label label-danger">Vencida</span>
				<span class="label label-default">Por vencer</span>
	    	</div>
	    	
	    	<?= sqlToHtml($qr,array('id','#Cuota','vencimiento','Importe'),array(),array(
				'#Cuota'=>function($val,$row){
					$label = 'default';
					if($row->pagado==1){
						$label = 'success';
					}
					elseif(strtotime($row->fecha_a_pagar)<time()){
						$label = 'danger';
					}
					return '<span class="label label-'.$label.'">'.$val.'</span>';
				},
				'Importe'=>function($val,$row){
					$importe = $val;
					//Traer pagos 
					$pagado = get_instance()->db->query("SELECT IFNULL(SUM(total_pagado),0) as saldo FROM pagocliente WHERE plan_credito_id = '".$row->id."'")->row()->saldo;
					return number_format($importe-$pagado,0,',','.');
				}
	    	)) ?>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="profile">
	    	<div style="text-align: right">
	    		Cuota <span id="spanCuotaNumero">#2</span>
	    	</div>
	    	<form  method="post" id="crudForm" onsubmit="return pagarCredito(this)">
	                <div class="form-group" id="total_credito_field_box">
	                	<label for="field-total_credito" id="total_credito_display_as_box" style="width:100%">
	                		Total a cobrar:
	                	</label>
	                	<input type="text" name="total_credito" value="61111" id="field-total_credito" class="form-control" readonly="">
	                </div>		                		                
	                <div class="form-group" id="total_pagado_field_box">
	                	<label for="field-total_pagado" id="total_pagado_display_as_box" style="width:100%">
	                		Total cobrado<span class="required">*</span>  :
	                	</label>
	                	<input type="text" name="total_pagado" value="61111" id="field-total_pagado" class="form-control">
	                </div>
	                <input id="field-recibo" type="hidden" name="recibo" value="0">
	                <input id="field-formapago_id" type="hidden" name="formapago_id" value="<?= $credito->forma_pago_id ?>">
	                <input id="field-concepto" type="hidden" name="concepto" value="Abono de cuota">
	                <input id="field-total_mora" type="hidden" name="total_mora" value="0">
	                <input id="field-clientes_id" type="hidden" name="clientes_id" value="<?= $credito->cliente_id ?>">
	                <input id="field-cliente" type="hidden" name="cliente" value="<?= $credito->cliente ?>">
	                <input id="field-fecha" type="hidden" name="fecha" value="<?= date("Y-m-d H:i:s") ?>">
	                <input id="field-creditos_id" type="hidden" name="creditos_id" value="<?= $_GET['creditos_id'] ?>">
	                <input id="field-plan_credito_id" type="hidden" name="plan_credito_id" value="0">
	                <input id="field-sucursal" type="hidden" name="sucursal" value="<?= $this->user->sucursal ?>">
	                <input id="field-caja" type="hidden" name="caja" value="<?= $this->user->caja ?>">
	                <input id="field-user" type="hidden" name="user" value="<?= $this->user->id ?>">
	                <input id="field-cajadiaria" type="hidden" name="cajadiaria" value="<?= $this->user->cajadiaria ?>">
	                <input id="field-anulado" type="hidden" name="anulado" value="0">
	                <!-- End of hidden inputs -->		                
	                <div id="report-success"></div>
					<div class="btn-group">			
	                    <button id="form-button-save" type="submit" class="btn btn-success">PAGAR</button>		                    		                    
					</div>                
			</form>
	    </div>
	  </div>

	</div>
	<script>
		var plan = 0;
		$("#creditoCobro #clienteModal").html('<?= $credito->cliente ?>');
		$("#CreditoSaldo").html('<?= $credito->saldo ?>')
		$(".tab-pane#home table tr").on('click',function(){
			var id = $($(this).find('td')[0]).html();
			var cuota = $($(this).find('td')[1]).text();			
			var importe = $($(this).find('td')[3]).html();
			importe = importe.replace(/\./g,'');
			importe = importe.replace(/,/g,'.');
			if(!isNaN(parseInt(id))){
				$("#cargarPagoTabBtn").show();
				$('#cargarPagoTabBtn a[href="#profile"]').tab('show');
				$("#spanCuotaNumero").html('#'+cuota);
				$("#field-plan_credito_id").val(id);
				plan = id;
				$("#field-total_credito").val(importe);
				$("#field-total_pagado").val(importe);
			}
		});

		function pagarCredito(f){			
			insertar('movimientos/creditos/pagar_credito/'+plan+'/insert',f,'#report-success',function(data){
				$('#report-success').html(data.success_message);
				if(data.success){
					verCliente('<?= $_GET['creditos_id'] ?>');
				}
			});
			return false;
		}
	</script>
<?php endif ?>