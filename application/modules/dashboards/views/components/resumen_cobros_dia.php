<div id="resumenCobrosDiaContent">
<?php 
    $year = empty($_POST['year'])?date("m"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT 
format(sum(consulta1.uno),0,'de_DE') as '1', 
format(sum(consulta1.dos),0,'de_DE') as '2', 
format(sum(consulta1.tres),0,'de_DE') as '3', 
format(sum(consulta1.cuat),0,'de_DE') as '4', 
format(sum(consulta1.cinc),0,'de_DE') as '5', 
format(sum(consulta1.seis),0,'de_DE') as '6', 
format(sum(consulta1.siet),0,'de_DE') as '7', 
format(sum(consulta1.och),0,'de_DE') as '8', 
format(sum(consulta1.nuev),0,'de_DE') as '9', 
format(sum(consulta1.diez),0,'de_DE') as '10', 
format(sum(consulta1.onc),0,'de_DE') as '11', 
format(sum(consulta1.doc),0,'de_DE') as '12',
format(sum(consulta1.trec),0,'de_DE') as '13', 
format(sum(consulta1.cator),0,'de_DE') as '14', 
format(sum(consulta1.quin),0,'de_DE') as '15', 
format(sum(consulta1.dies6),0,'de_DE') as '16', 
format(sum(consulta1.dies7),0,'de_DE') as '17', 
format(sum(consulta1.dies8),0,'de_DE') as '18', 
format(sum(consulta1.dies9),0,'de_DE') as '19', 
format(sum(consulta1.veint),0,'de_DE') as '20', 
format(sum(consulta1.veint1),0,'de_DE') as '21', 
format(sum(consulta1.veint2),0,'de_DE') as '22', 
format(sum(consulta1.veint3),0,'de_DE') as '23', 
format(sum(consulta1.veint4),0,'de_DE') as '24',
format(sum(consulta1.veint5),0,'de_DE') as '25', 
format(sum(consulta1.veint6),0,'de_DE') as '26', 
format(sum(consulta1.veint7),0,'de_DE') as '27', 
format(sum(consulta1.veint8),0,'de_DE') as '28', 
format(sum(consulta1.veint9),0,'de_DE') as '29', 
format(sum(consulta1.treint),0,'de_DE') as '30', 
format(sum(consulta1.treint1),0,'de_DE') as '31'   

FROM(
SELECT 
if(consulta.dia=01,consulta.total,0) as uno, 
if(consulta.dia=02,consulta.total,0) as dos, 
if(consulta.dia=03,consulta.total,0) as tres, 
if(consulta.dia=04,consulta.total,0) as cuat, 
if(consulta.dia=05,consulta.total,0) as cinc, 
if(consulta.dia=06,consulta.total,0) as seis, 
if(consulta.dia=07,consulta.total,0) as siet, 
if(consulta.dia=08,consulta.total,0) as och, 
if(consulta.dia=09,consulta.total,0) as nuev, 
if(consulta.dia=10,consulta.total,0) as diez, 
if(consulta.dia=11,consulta.total,0) as onc, 
if(consulta.dia=12,consulta.total,0) as doc,
if(consulta.dia=13,consulta.total,0) as trec,
if(consulta.dia=14,consulta.total,0) as cator, 
if(consulta.dia=15,consulta.total,0) as quin, 
if(consulta.dia=16,consulta.total,0) as dies6, 
if(consulta.dia=17,consulta.total,0) as dies7, 
if(consulta.dia=18,consulta.total,0) as dies8, 
if(consulta.dia=19,consulta.total,0) as dies9, 
if(consulta.dia=20,consulta.total,0) as veint, 
if(consulta.dia=21,consulta.total,0) as veint1, 
if(consulta.dia=22,consulta.total,0) as veint2, 
if(consulta.dia=23,consulta.total,0) as veint3, 
if(consulta.dia=24,consulta.total,0) as veint4, 
if(consulta.dia=25,consulta.total,0) as veint5,    
if(consulta.dia=26,consulta.total,0) as veint6,
if(consulta.dia=27,consulta.total,0) as veint7,
if(consulta.dia=28,consulta.total,0) as veint8, 
if(consulta.dia=29,consulta.total,0) as veint9,
if(consulta.dia=30,consulta.total,0) as treint,
if(consulta.dia=31,consulta.total,0) as treint1      
FROM(
        SELECT 
MONTH(NOW()) mes_act,
month(pagocliente.fecha) mes,
day(date(pagocliente.fecha)) dia,
pagocliente.total_pagado as total  
FROM pagocliente
INNER JOIN creditos on creditos.id=pagocliente.creditos_id
WHERE (creditos.anulado = 0 or creditos.anulado is null) and pagocliente.anulado=0 and month(pagocliente.fecha) = '".$year."') as consulta) consulta1
    ")->row();
?>

<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-bar-chart"></i> Resumen de cobros por día (<?= meses($year) ?>)</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Mes</b></a>
                            </li>              
                            <?php for($i = date("m");$i<=12;$i++): ?>
                            <li>
                                <a href="javascript:changeYearresumenCobrosDia(<?= $i ?>)"><?= meses($i) ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
                        <div id="resumenCobrosDia"></div>
                    </div>
                </div>
            </div>
</div>

<script>
    Morris.Bar({
      element: 'resumenCobrosDia',
      data:<?php 
        $data = array();
        foreach($resumen as $n=>$v){
            $data[] = array('y'=>$n,'a'=>$v);
        }
        echo json_encode($data);
      ?>,
      xkey: 'y',
      ykeys: ['a'],
      labels: [''],
      hoverCallback: function (index, options, content, row) {
          return row.y+': '+currencyFormat(row.a);
      }
    });

    function changeYearresumenCobrosDia(y){
        $.post('dashboards/refresh/resumen_cobros_dia',{year:y},function(data){
            $("#resumenCobrosDiaContent").html(data);
        });
    }
</script>
</div>