<div id="resumenCapitalMesContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT 
		format(sum(consulta1.enero),0,'de_DE') as Enero, 
		format(sum(consulta1.febrero),0,'de_DE') as Febrero, 
		format(sum(consulta1.marzo),0,'de_DE') as Marzo, 
		format(sum(consulta1.abril),0,'de_DE') as Abril, 
		format(sum(consulta1.mayo),0,'de_DE') as Mayo, 
		format(sum(consulta1.junio),0,'de_DE') as Junio, 
		format(sum(consulta1.julio),0,'de_DE') as Julio, 
		format(sum(consulta1.agosto),0,'de_DE') as Agosto, 
		format(sum(consulta1.setiembre),0,'de_DE') as Setiembre, 
		format(sum(consulta1.octubre),0,'de_DE') as Octubre, 
		format(sum(consulta1.noviembre),0,'de_DE') as Noviembre, 
		format(sum(consulta1.diciembre),0,'de_DE') as Diciembre 
		FROM(
		SELECT 
		if(consulta.mes=01,consulta.total,0) as enero, 
		if(consulta.mes=02,consulta.total,0) as febrero, 
		if(consulta.mes=03,consulta.total,0) as marzo, 
		if(consulta.mes=04,consulta.total,0) as abril, 
		if(consulta.mes=05,consulta.total,0) as mayo, 
		if(consulta.mes=06,consulta.total,0) as junio, 
		if(consulta.mes=07,consulta.total,0) as julio, 
		if(consulta.mes=08,consulta.total,0) as agosto, 
		if(consulta.mes=09,consulta.total,0) as setiembre, 
		if(consulta.mes=10,consulta.total,0) as octubre, 
		if(consulta.mes=11,consulta.total,0) as noviembre, 
		if(consulta.mes=12,consulta.total,0) as diciembre,
		consulta.fecha  
		FROM(
		    SELECT 
		   month(creditos.fecha_credito) mes,
		   creditos.monto_credito as total,
		   creditos.fecha_credito as fecha
		    FROM creditos 
		WHERE (creditos.anulado = 0 or creditos.anulado is null)) as consulta) consulta1 WHERE YEAR(consulta1.fecha) = '".$year."'
    ")->row();
?>

<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-bar-chart"></i> Resumen capital por mes (<?= $year ?>)</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:changeYearresumenCapitalMes(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main">                        
						<div id="resumenCapitalMes"></div>
                    </div>
                </div>
            </div>
</div>

<script>
	Morris.Bar({
	  element: 'resumenCapitalMes',
      data:<?php 
        $data = array();
        foreach($resumen as $n=>$v){
            $data[] = array('y'=>$n,'a'=>$v);
        }
        echo json_encode($data);
      ?>,
	  xkey: 'y',
	  ykeys: ['a'],
	  labels: [''],
      hoverCallback: function (index, options, content, row) {
          return row.y+': '+currencyFormat(row.a);
      }
	});

    function changeYearresumenCapitalMes(y){
        $.post('dashboards/refresh/resumen_capital_mes',{year:y},function(data){
            $("#resumenCapitalMesContent").html(data);
        });
    }
</script>
</div>