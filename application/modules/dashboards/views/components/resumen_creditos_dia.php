
<?php
    $qry = $this->db->query("        
		      SELECT 
        '1' AS nro,
        creditos.id as id,
        CONCAT_WS(' ',clientes.nombres,clientes.apellidos) as cliente,
        format(creditos.total_credito,0, 'de_DE') as total  
        FROM creditos 
        INNER JOIN clientes on clientes.id=creditos.clientes_id  
        INNER JOIN formapago on formapago.id=creditos.forma_pago_id 
        WHERE date(creditos.fecha_credito)=date(now())
        UNION ALL 
        SELECT 
        '',
        'Total',
        '',
        FORMAT(SUM(creditos.total_credito),0,'de_DE')
        FROM creditos 
        INNER JOIN clientes on clientes.id=creditos.clientes_id  
        INNER JOIN formapago on formapago.id=creditos.forma_pago_id 
        WHERE date(creditos.fecha_credito)=date(now())
    ");
?>
<div class="widget-color-dark widget-box ui-sortable-handle" data-id="4">
   
            <div class="widget-header">
                <h5 class="widget-title"><i class="ace-icon fa fa-list"></i> Resumen de créditos de hoy</h5>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="widget-main no-padding" style="max-height: 371px;overflow: auto;">
                        <?php sqlToHtml($qry,array('nro','cliente','id','total'),array('nro'=>'NRO','id'=>'#Credito','total'=>'Total a pagar'),array('nro'=>function($val,$row,$nro){return $nro+1;})); ?>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
