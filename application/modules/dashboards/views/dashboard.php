<style>
	h4{
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
</style>
<script src="<?= base_url() ?>js/raphael-min.js"></script>
<script src="<?= base_url() ?>js/prettify.min.js"></script>
<script src="<?= base_url().'js/morris.js' ?>"></script>                                
<link rel="stylesheet" href="<?= base_url() ?>css/prettify.min.css">
<link rel="stylesheet" href="<?= base_url().'css/morris.css' ?>">
<div class="row">
	<div class="col-xs-12">
  		<?= $this->load->view('components/accesos_directos',array(),TRUE,'dashboards'); ?>
  	</div>

</div>

<div class="row">
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/resumen_facturaciones',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/informaciones_generales',array(),TRUE,'dashboards') ?>
	</div>	
</div>

<div class="row">
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/resumen_venta_mes',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/compras_por_mes',array(),TRUE,'dashboards') ?>
	</div>	
</div>
<div class="row">
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/pago_por_fecha',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/totales_por_categoria',array(),TRUE,'dashboards') ?>
	</div>	
</div>
<div class="row">
	<div class="col-xs-12 col-md-6">
		<?= $this->load->view('components/top_10_productos_mas_vendidos_admin',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-xs-12 col-md-6">		
	</div>
</div>
