<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Sin procesar</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Aprobadas</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Entregadas</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Rechazadas</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">

	<?php 
		$crud = new ajax_grocery_crud();
		$crud->set_table('transferencias')
			 ->set_theme('bootstrap2')
			 ->columns('id','sucursal_origen','sucursal_destino','fecha_solicitud','procesado','user_id')
			 ->display_as('procesado', 'Status')
			 ->set_relation('sucursal_origen', 'sucursales', 'denominacion')
             ->set_relation('sucursal_destino', 'sucursales', 'denominacion')
             ->unset_delete();
        $crud->set_url('movimientos/productos/transferencias/0/');
		$crud = $crud->render();
		echo $crud->output;             
	?>

    </div>
    <div role="tabpanel" class="tab-pane" id="profile">

	<?php 
		$crud = new ajax_grocery_crud();
		$crud->set_table('transferencias')
			 ->set_theme('bootstrap2')
			 ->columns('id','sucursal_origen','sucursal_destino','fecha_solicitud','procesado','user_id')
			 ->display_as('procesado', 'Status')
			 ->set_relation('sucursal_origen', 'sucursales', 'denominacion')
             ->set_relation('sucursal_destino', 'sucursales', 'denominacion')
             ->unset_delete();
        $crud->set_url('movimientos/productos/transferencias/1/');
		$crud = $crud->render();
		echo $crud->output;             
	?>

    </div>
    <div role="tabpanel" class="tab-pane" id="messages">

	<?php 
		$crud = new ajax_grocery_crud();
		$crud->set_table('transferencias')
			 ->set_theme('bootstrap2')
			 ->columns('id','sucursal_origen','sucursal_destino','fecha_solicitud','procesado','user_id')
			 ->display_as('procesado', 'Status')
			 ->set_relation('sucursal_origen', 'sucursales', 'denominacion')
             ->set_relation('sucursal_destino', 'sucursales', 'denominacion')
             ->unset_delete();
        $crud->set_url('movimientos/productos/transferencias/2/');
		$crud = $crud->render();
		echo $crud->output;             
	?>

    </div>
    <div role="tabpanel" class="tab-pane" id="settings">

	<?php 
		$crud = new ajax_grocery_crud();
		$crud->set_table('transferencias')
			 ->set_theme('bootstrap2')
			 ->columns('id','sucursal_origen','sucursal_destino','fecha_solicitud','procesado','user_id')
			 ->display_as('procesado', 'Status')
			 ->set_relation('sucursal_origen', 'sucursales', 'denominacion')
             ->set_relation('sucursal_destino', 'sucursales', 'denominacion')
             ->unset_delete();
        $crud->set_url('movimientos/productos/transferencias/-1/');
		$crud = $crud->render();
		echo $crud->output;             
	?>

    </div>
  </div>

</div>

<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/productos/transferencias_detalles/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}
</script>