<?php if($this->user->admin==1): ?>
	<a href="<?= base_url('movimientos/compras/compradetalles') ?>" class="btn btn-info">Ver más detalles</a>
<?php endif ?>
<?= $output ?>

<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/compras/compras_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}

	function anular(id){
        if(confirm('Seguro que desea anular esta factura?')){
            $.post('<?= base_url('json/anular_venta') ?>',{id:id},function(data){
                emergente(data);
            });
        }
    }
</script>