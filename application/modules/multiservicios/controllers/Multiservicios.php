<?php

require_once APPPATH.'/controllers/Panel.php';    

class Multiservicios extends Panel {

    function __construct() {
        parent::__construct();             
    }

    function ajuste_servicio_cuentas(){
    	$crud = $this->crud_function('','');
        $crud->field_type('user_id','hidden',$this->user->id)             
             ->field_type('fecha_hora','hidden',date("Y-m-d H:i:s"))
             ->field_type('anulado','hidden',0)
             ->field_type('tipo_ajuste','dropdown',array('-'=>'-','+'=>'+'))
             ->display_as('servicio_empresas_id','Empresa')
             ->display_as('servicio_cuentas_id','Cuenta')
             ->set_relation('servicio_empresas_id','servicio_empresas','denominacion')
             ->set_relation('servicio_cuentas_id','servicio_cuentas','denominacion')
             ->set_relation_dependency('servicio_cuentas_id','servicio_empresas_id','servicio_empresas_id');
    	$crud = $crud->render();
    	$this->loadView($crud);
    }



    function servicio_mov_general(){
    	$crud = $this->crud_function('','');
    	$crud = $crud->render();
    	$this->loadView($crud);
    }

    function servicio_tipo_transacciones(){
    	$crud = $this->crud_function('','');
    	$crud = $crud->render();
    	$this->loadView($crud);
    }

    function servicio_empresas(){
    	$crud = $this->crud_function('','');
    	$crud = $crud->render();
    	$this->loadView($crud);
    }

     function servicio_cuentas(){
        $crud = $this->crud_function('','');
        $crud->display_as('servicio_mov_general_id','Movimiento')
             ->display_as('servicio_empresas_id','Empresa')
             ->display_as('servicio_numeros_id','Numero')
             ->set_relation('servicio_empresas_id','servicio_empresas','denominacion')
             ->set_relation('servicio_numeros_id','servicio_numeros','{alias}\<{numero}\>')
             ->set_relation_dependency('servicio_numeros_id','servicio_empresas_id','servicio_empresas_id');
        if(!empty($_POST['serv_mov_general']) && !empty($_POST['serv_empresa'])){
            $crud->where('servicio_cuentas.servicio_mov_general_id',$_POST['serv_mov_general']);
            $crud->where('servicio_cuentas.servicio_empresas_id',$_POST['serv_empresa']);
        }
        $crud = $crud->render();
        $this->loadView($crud);
    }

    function servicio_acreditaciones(){
    	$crud = $this->crud_function('','');
    	$crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
    	$crud->field_type('user_id','hidden',$this->user->id)
             ->field_type('fecha_hora','hidden',date("Y-m-d H:i:s"))
             ->field_type('anulado','true_false',array('0'=>'NO','1'=>'SI'))
    		 ->display_as('servicio_empresas_id','Empresa')
    		 ->display_as('servicio_cuentas_id','Cuenta')
    		 ->display_as('servicio_mov_general_id','Movimiento')
    		 ->display_as('servicio_tipo_transacciones_id','Transacción')
    		 ->unset_columns('user_id','cajadiaria_id')
             ->set_relation('servicio_cuentas_id','servicio_cuentas','denominacion')
             ->set_relation('servicio_empresas_id','servicio_empresas','denominacion')
             ->set_relation_dependency('servicio_cuentas_id','servicio_empresas_id','servicio_empresas_id');
    	$crud = $crud->render();
    	$this->loadView($crud);
    }

    function servicios_web(){
    	$crud = $this->crud_function('','');
    	$crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
    	$crud->field_type('user_id','hidden',$this->user->id)   
             ->field_type('fecha_hora','hidden',date("Y-m-d H:i:s"))
             ->field_type('anulado','hidden',0) 
             ->field_type('servicio_mov_general_id','hidden',3)         
    		 ->display_as('servicio_empresas_id','Empresa')
    		 ->display_as('servicio_cuentas_id','Cuenta')
    		 ->display_as('servicio_mov_general_id','Movimiento')
    		 ->display_as('servicio_tipo_transacciones_id','Transacción')
    		 ->unset_columns('user_id','cajadiaria_id')
             ->set_relation('servicio_cuentas_id','servicio_cuentas','denominacion')
             ->set_relation('servicio_empresas_id','servicio_empresas','denominacion');
    	$crud = $crud->render();
        $crud->output = $this->load->view('servicios_web',array('output'=>$crud->output),TRUE);
    	$this->loadView($crud);
    }
    
    function servicio_giros(){
    	$crud = $this->crud_function('','');
    	$crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
    	$crud->field_type('user_id','hidden',$this->user->id)
             ->field_type('fecha_hora','hidden',date("Y-m-d H:i:s"))
             ->field_type('anulado','hidden',0)
             ->field_type('servicio_mov_general_id','hidden',1)         
    		 ->display_as('servicio_empresas_id','Empresa')
    		 ->display_as('servicio_cuentas_id','Cuenta')
    		 ->display_as('servicio_mov_general_id','Movimiento')
    		 ->display_as('servicio_tipo_transacciones_id','Transacción')
    		 ->unset_columns('user_id','cajadiaria_id')
             ->set_relation('servicio_cuentas_id','servicio_cuentas','denominacion')
             ->set_relation('servicio_empresas_id','servicio_empresas','denominacion');
    	$crud = $crud->render();
        $crud->output = $this->load->view('servicios_web',array('output'=>$crud->output),TRUE);
    	$this->loadView($crud);
    }
    function servicio_numeros(){
    	$crud = $this->crud_function('','');
    	$crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
    	$crud->field_type('user_id','hidden',$this->user->id)
    		 ->display_as('servicio_empresas_id','Empresa')
    		 ->display_as('servicio_cuentas_id','Cuenta')
    		 ->display_as('servicio_mov_general_id','Movimiento')
    		 ->display_as('servicio_tipo_transacciones_id','Transacción')
    		 ->unset_columns('user_id','cajadiaria_id');
    	$crud = $crud->render();
    	$this->loadView($crud);
    }
    function servicio_telefonia($x = ''){
        if($x == 'getSaldo'){
            $saldo = array('saldo'=>0);
            if(!empty($_POST['scid'])){
                $saldo['saldo'] = $this->db->query('SELECT saldo_telefonia('.$_POST['scid'].') as total')->row()->total;
            }
            echo json_encode($saldo);
            die();
        }
    	$crud = $this->crud_function('','');
    	$crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
    	$crud->field_type('user_id','hidden',$this->user->id)             
             ->field_type('fecha_hora','hidden',date("Y-m-d H:i:s"))
             ->field_type('anulado','hidden',0)
             ->field_type('servicio_mov_general_id','hidden',2)
    		 ->display_as('servicio_empresas_id','Empresa')
    		 ->display_as('servicio_cuentas_id','Cuenta')
    		 ->display_as('servicio_mov_general_id','Movimiento')
    		 ->display_as('servicio_tipo_transacciones_id','Transacción')
    		 ->unset_columns('user_id','cajadiaria_id')
             ->set_relation('servicio_cuentas_id','servicio_cuentas','denominacion')
             ->set_relation('servicio_empresas_id','servicio_empresas','denominacion');
    	$crud = $crud->render();
        $crud->output = $this->load->view('servicios_telefonia',array('output'=>$crud->output),TRUE);
    	$this->loadView($crud);
    }

    function servicios_saldos(){
        $output = $this->load->view('saldos',array(),TRUE);
        $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$output));
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
